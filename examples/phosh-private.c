/*
 * Copyright (C) 2022 Purism SPC
 *               2025 The Phosh Developers
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Author: Guido Günther <agx@sigxcpu.org>
 *
 * Based on wlroots layer-shell example which is BSD licensed.
 */

#define G_LOG_DOMAIN "phoc-example"

#include "ext-foreign-toplevel-list-v1-client-protocol.h"

#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "phosh-private-client-protocol.h"

#include <glib.h>


typedef struct _Toplevel {
  struct ext_foreign_toplevel_handle_v1 *handle;
  char    *app_id;
  char    *title;
  char    *identifier;
  guint32  pid;
} Toplevel;


static struct wl_display *display;
static struct phosh_private *phosh_private;
static struct ext_foreign_toplevel_list_v1 *toplevel_list;
static GHashTable *toplevels;

static Toplevel *
toplevel_new (void)
{
  return g_new0 (Toplevel, 1);
}


static void
toplevel_destroy (Toplevel *toplevel)
{
  g_clear_pointer (&toplevel->title, g_free);
  g_clear_pointer (&toplevel->app_id, g_free);
  g_clear_pointer (&toplevel->identifier, g_free);

  g_free (toplevel);
}


static void
print_toplevel (Toplevel *toplevel)
{
  g_print ("New toplevel %p: title: %s, app_id: %s, identifier: %s, pid %u\n",
           toplevel->handle,
           toplevel->title,
           toplevel->app_id,
           toplevel->identifier,
           toplevel->pid);
}


static void
handle_pid (void                                  *data,
            struct phosh_private                  *phosh_private_,
            struct ext_foreign_toplevel_handle_v1 *toplevel_handle,
            uint32_t                               pid)


{
  Toplevel *toplevel = g_hash_table_lookup (toplevels, toplevel_handle);

  g_assert (toplevel);
  toplevel->pid = pid;
  print_toplevel (toplevel);
}


static const struct phosh_private_listener phosh_private_listener = {
  .pid = handle_pid,
};


static void
handle_closed (void                                  *data,
               struct ext_foreign_toplevel_handle_v1 *toplevel_handle)
{
  g_debug ("Toplevel %p closed", toplevel_handle);
  g_assert (g_hash_table_remove (toplevels, toplevel_handle));
}


static void
handle_done (void                                  *data,
             struct ext_foreign_toplevel_handle_v1 *toplevel_handle)
{
  Toplevel *toplevel = data;

  print_toplevel (toplevel);
}


static void
handle_title (void                                  *data,
              struct ext_foreign_toplevel_handle_v1 *toplevel_handle,
              const char                            *title)
{
  Toplevel *toplevel = data;

  toplevel->title = g_strdup (title);
}


static void
handle_app_id (void                                  *data,
               struct ext_foreign_toplevel_handle_v1 *toplevel_handle,
               const char                            *app_id)
{
  Toplevel *toplevel = data;

  toplevel->app_id = g_strdup (app_id);
}


static void
handle_identifier (void                                  *data,
                   struct ext_foreign_toplevel_handle_v1 *toplevel_handle,
                   const char                            *identifier)
{
  Toplevel *toplevel = data;

  toplevel->identifier = g_strdup (identifier);
}


struct ext_foreign_toplevel_handle_v1_listener toplevel_handle_listener = {
  .closed = handle_closed,
  .done = handle_done,
  .title = handle_title,
  .app_id = handle_app_id,
  .identifier = handle_identifier,
};


static void
handle_toplevel (void                                  *data,
                 struct ext_foreign_toplevel_list_v1   *toplevel_list_,
                 struct ext_foreign_toplevel_handle_v1 *toplevel_handle)
{
  Toplevel *toplevel = toplevel_new ();

  g_print ("New toplevel: %p\n", toplevel_handle);
  toplevel->handle = toplevel_handle;
  g_hash_table_insert (toplevels, toplevel->handle, toplevel);

  ext_foreign_toplevel_handle_v1_add_listener (toplevel->handle,
                                               &toplevel_handle_listener,
                                               toplevel);
}


static void
handle_finished (void                                *data,
                 struct ext_foreign_toplevel_list_v1 *toplevel_list_)
{
  /* Nothing to do */
}


const struct ext_foreign_toplevel_list_v1_listener toplevel_list_listener = {
  .toplevel = handle_toplevel,
  .finished = handle_finished,
};


static void
handle_global (void               *data,
               struct wl_registry *registry,
               uint32_t            name,
               const char         *interface,
               uint32_t            version)
{
  if (g_str_equal (interface, phosh_private_interface.name)) {
    phosh_private = wl_registry_bind (registry, name,
                                      &phosh_private_interface, 8);
    phosh_private_add_listener (phosh_private, &phosh_private_listener, NULL);
  } else if (g_str_equal (interface, ext_foreign_toplevel_list_v1_interface.name)) {
    toplevel_list = wl_registry_bind (registry, name,
                                      &ext_foreign_toplevel_list_v1_interface,
                                      1);
    ext_foreign_toplevel_list_v1_add_listener (toplevel_list,
                                               &toplevel_list_listener,
                                               NULL);
  }
}


static void
handle_global_remove (void *data, struct wl_registry *registry, uint32_t name)
{
  // who cares
}


static const struct wl_registry_listener registry_listener = {
  .global = handle_global,
  .global_remove = handle_global_remove,
};


int
main (int argc, char **argv)
{
  struct wl_registry *registry;

  display = wl_display_connect (NULL);
  if (display == NULL) {
    g_critical ("Failed to create display");
    return 1;
  }

  toplevels = g_hash_table_new_full (g_direct_hash,
                                     g_direct_equal,
                                     NULL,
                                     (GDestroyNotify)toplevel_destroy);
  registry = wl_display_get_registry (display);
  wl_registry_add_listener (registry, &registry_listener, NULL);
  wl_display_roundtrip (display);

  if (phosh_private == NULL) {
    g_critical ("phosh_private not available");
    return 1;
  }

  g_message ("Press CTRL-C to quit");
  phosh_private_set_shell_state (phosh_private, PHOSH_PRIVATE_SHELL_STATE_UP);
  wl_display_roundtrip (display);

  while (wl_display_dispatch (display)) {
    // This space intentionally left blank
  }

  return 0;
}
